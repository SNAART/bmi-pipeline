//
//  ViewController.swift
//  Lab3
//
//  Created by Tran Nham on 29.3.2020.
//  Copyright © 2020 Tran Nham. All rights reserved.
//

import UIKit
import os.log


class BMIViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var heightPickerView: UIPickerView!
    @IBOutlet weak var weightPickerView: UIPickerView!
    @IBOutlet weak var viewHistoryButton: UIBarButtonItem!
    
    //MARK: Properties

    var listBMI = [Person]()
    private var height = 0.0
    private var weight = 0.0
    private var bmiIndex = 0.0
    private var name = ""
    
    var newPerson : Person?
    
    private let heightDataSource = Array(150...200)
    private let weightDataSource = Array(50...200)

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
                
        nameText.delegate = self
        
        heightPickerView.dataSource = self
        heightPickerView.delegate = self
        
        weightPickerView.dataSource = self
        weightPickerView.delegate = self

        // Enable the Go button only if the text field has a valid one
        updateGoButtonState()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == heightPickerView {
            return heightDataSource.count
        }else if pickerView == weightPickerView {
            return weightDataSource.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == heightPickerView {
            return String(heightDataSource[row])
        }else if pickerView == weightPickerView {
            return String(weightDataSource[row])
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == heightPickerView {
            height = Double(heightDataSource[row])
            self.view.endEditing(false)
        }else if pickerView == weightPickerView {
            weight = Double(weightDataSource[row])
            self.view.endEditing(false)
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let allowedChar = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM"
        
        let allowCharSet = CharacterSet(charactersIn: allowedChar)
        
        let typeCharSet = CharacterSet(charactersIn: string)
        
        return allowCharSet.isSuperset(of: typeCharSet)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField.text?.count != 0) {
            textField.resignFirstResponder()
            name = textField.text ?? ""
            updateGoButtonState()
            return true
        }
        let alert = UIAlertController(title: "Not allowed", message: "Please enter your name", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

        self.present(alert, animated: true)
        return false
    }
    
    
    //MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        if let navigationController = segue.destination as? UINavigationController
        {

            let tableViewController = navigationController.topViewController as! HistoryTableViewController
                tableViewController.tempList = listBMI
        }
    }
    
    //MARK: Action

    @IBAction func computeBMI(_ sender: UIButton) {
        //Create instance base on values
        newPerson = Person(name, height/100, weight)
        bmiIndex = newPerson!.calBMI()
        
        if let new = newPerson {
            listBMI.append(new)
        }else{
            print("Anything wrong")
        }
        
        //Change color based on BMI
        
        if let newPerson = newPerson {
            switch newPerson.bmi {
            case 1..<18.5:
                view.backgroundColor = UIColor.systemPurple
            case 18.5..<25:
                view.backgroundColor = UIColor.systemGreen
            case 25..<30:
                view.backgroundColor = UIColor.systemPink
            case 30..<100:
                view.backgroundColor = UIColor.systemRed
            default:
                view.backgroundColor = UIColor.white
            }
        }
        
        //Calculate the BMi and display it on label
        resultLabel.text = "\(name)'s BMI is : \(String(format: "%.1f", bmiIndex))"
            
        //Disable button and reset the name, weight, height to initial value
        resetValue()
    }
    
    //MARK: Private Methods
    private func updateGoButtonState() {
        // Disable the Save button if the text field is empty.
        let text = nameText.text ?? ""
        goButton.isEnabled = !text.isEmpty
    }
    
    private func resetValue() {
        goButton.isEnabled = false
        name = ""
        height = 0.0
        weight = 0.0
        nameText.text = name
    }
    
}


