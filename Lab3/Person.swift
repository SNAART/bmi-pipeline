//
//  BMIModel.swift
//  Lab3
//
//  Created by Tran Nham on 29.3.2020.
//  Copyright © 2020 Tran Nham. All rights reserved.
//

import Foundation

class Person {
    var height : Double
    var weight : Double
    var name : String
    var bmi : Double
    
    init(_ name : String,_ height : Double,_ weight : Double,_ bmi : Double = 0.0) {
        self.name = name
        self.height = height
        self.weight = weight
        self.bmi = bmi
    }
    
    func calBMI()-> Double {
        bmi = weight/(height*height)
        return bmi
    }
}
